# emacs-guile

Emacs configuration file focusing on Guile development

Add this line to your emacs configuration file to get ready to hack with Guile !

```lisp
(eval-and-compile 
  (load (expand-file-name "/path/to/guile-ide.el" user-emacs-directory)))
```

Features :
- [ ] No errors and no warning out of the box when Emacs start and when using following features :
- [ ] Install from **Guix** and **MELPA**?
- [ ] **Magit** git versionning
- [ ] **Geiser** REPL, go to documentation
- [ ] **?** go to definition in Guile source code (even Guile's modules, srfi, ...)
- [ ] **Paredit** code manipulation facilities
- [ ] **?guile-fmt?** code formatting according style guidelines (does Guilers have some?)
- [ ] **emacs-refactor.el** refactoring facilities like extract functions, variables, ...
- [ ] **YASnippet** provide template (GPL insertion, test harness skeleton, ...)
- [ ] **?** Debugging facilities (stepper, breakpoints, inspection, ...)
