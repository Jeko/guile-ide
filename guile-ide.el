;;This file is part of Emacs-Guile
;;
;;    Emacs-Guile is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;
;;    Emacs-Guile is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.
;;
;;    You should have received a copy of the GNU General Public License
;;    along with Emacs-Guile  If not, see <https://www.gnu.org/licenses/>

(show-paren-mode 1)
(global-hl-line-mode +1)

;; guix install emacs-rainbow-delimiters
(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

;; guix install emacs-iedit
(require 'iedit)

;; ido mode
(setq ido-enable-flex-matching t)

;; guix install emacs-paredit
(require 'paredit)
(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook                  #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook                        #'enable-paredit-mode)
(add-hook 'lisp-mode-hook                        #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook            #'enable-paredit-mode)
(add-hook 'scheme-mode-hook                      #'enable-paredit-mode)

;; guix install emacs-auto-complete
(ac-config-default)

;; guix install emacs-geiser
(setq geiser-active-implementations '(guile))

;; guix install emacs-ac-geiser
(require 'ac-geiser)
(add-hook 'geiser-mode-hook 'ac-geiser-setup)
(add-hook 'geiser-repl-mode-hook 'ac-geiser-setup)
(eval-after-load "auto-complete"
  '(add-to-list 'ac-modes 'geiser-repl-mode))

;; guix install emacs-yasnippet
(require 'yasnippet)
(yas-reload-all)
(add-hook 'prog-mode-hook #'yas-minor-mode)

;; M-x package-install RET emr
(if (not (package-installed-p 'emr))
    (progn
      (package-refresh-contents)
      (package-install 'emr)))
(require 'emr)
(autoload 'emr-show-refactor-menu "emr")
(define-key prog-mode-map (kbd "M-RET") 'emr-show-refactor-menu)
(eval-after-load "emr" '(emr-initialize))

;; Multilines !
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;; guix install emacs-projectile
(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

;; Fix indentation for my macros
(defun my-scheme-mode-hook ()
  (put 'describe 'scheme-indent-function 1)
  (put 'it 'scheme-indent-function 1)
  (put 'should 'scheme-indent-function 1)
  (put 'should-not 'scheme-indent-function 1)
  (put 'should= 'scheme-indent-function 1))
(add-hook 'scheme-mode-hook 'my-scheme-mode-hook)

;; Artanis template editing
(if (not (package-installed-p 'web-mode))
    (progn
      (package-refresh-contents)
      (package-install 'web-mode)))
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.html\\.tpl\\'" . web-mode))

;; Artanis don't like auto-saved files so I moved them in /tmp
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))
